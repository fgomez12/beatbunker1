<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "suenan".
 *
 * @property int $idsuenan
 * @property int $idinstrumento
 * @property int $idcancion
 *
 * @property Canciones $idcancion0
 * @property Instrumentos $idinstrumento0
 */
class Suenan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'suenan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idinstrumento', 'idcancion'], 'required'],
            [['idinstrumento', 'idcancion'], 'integer'],
            [['idinstrumento'], 'exist', 'skipOnError' => true, 'targetClass' => Instrumentos::class, 'targetAttribute' => ['idinstrumento' => 'idinstrumento']],
            [['idcancion'], 'exist', 'skipOnError' => true, 'targetClass' => Canciones::class, 'targetAttribute' => ['idcancion' => 'idcancion']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idsuenan' => 'Idsuenan',
            'idinstrumento' => 'Idinstrumento',
            'idcancion' => 'Idcancion',
        ];
    }

    /**
     * Gets query for [[Idcancion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdcancion0()
    {
        return $this->hasOne(Canciones::class, ['idcancion' => 'idcancion']);
    }

    /**
     * Gets query for [[Idinstrumento0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdinstrumento0()
    {
        return $this->hasOne(Instrumentos::class, ['idinstrumento' => 'idinstrumento']);
    }
}

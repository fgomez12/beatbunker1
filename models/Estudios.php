<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estudios".
 *
 * @property int $idestudio
 * @property string $nombre
 * @property string|null $web
 * @property string|null $tecnico_sonido
 * @property int $idcancion
 * @property int $idherramienta
 *
 * @property Canciones $idcancion0
 * @property Herramientas $idherramienta0
 */
class Estudios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estudios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'idcancion', 'idherramienta'], 'required'],
            [['idcancion', 'idherramienta'], 'integer'],
            [['nombre', 'web'], 'string', 'max' => 255],
            [['tecnico_sonido'], 'string', 'max' => 70],
            [['idcancion'], 'exist', 'skipOnError' => true, 'targetClass' => Canciones::class, 'targetAttribute' => ['idcancion' => 'idcancion']],
            [['idherramienta'], 'exist', 'skipOnError' => true, 'targetClass' => Herramientas::class, 'targetAttribute' => ['idherramienta' => 'idherramienta']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idestudio' => 'Idestudio',
            'nombre' => 'Nombre',
            'web' => 'Web',
            'tecnico_sonido' => 'Tecnico Sonido',
            'idcancion' => 'Idcancion',
            'idherramienta' => 'Idherramienta',
        ];
    }

    /**
     * Gets query for [[Idcancion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdcancion0()
    {
        return $this->hasOne(Canciones::class, ['idcancion' => 'idcancion']);
    }

    /**
     * Gets query for [[Idherramienta0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdherramienta0()
    {
        return $this->hasOne(Herramientas::class, ['idherramienta' => 'idherramienta']);
    }
}

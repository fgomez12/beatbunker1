<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "instrumentos".
 *
 * @property int $idinstrumento
 * @property string $tipo
 * @property string|null $marca
 * @property string|null $modelo
 * @property float|null $tamaño
 * @property string|null $material
 * @property string|null $url
 *
 * @property Instrumentoscanciones[] $instrumentoscanciones
 */
class Instrumentos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'instrumentos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo'], 'required'],
            [['tamaño'], 'number'],
            [['tipo', 'material', 'url'], 'string', 'max' => 255],
            [['marca', 'modelo'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idinstrumento' => 'Idinstrumento',
            'tipo' => 'Tipo',
            'marca' => 'Marca',
            'modelo' => 'Modelo',
            'tamaño' => 'Tamaño',
            'material' => 'Material',
            'url' => 'Url',
        ];
    }

    /**
     * Gets query for [[Instrumentoscanciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInstrumentoscanciones()
    {
        return $this->hasMany(Instrumentoscanciones::class, ['idinstrumento' => 'idinstrumento']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuarios".
 *
 * @property int $idusuario
 * @property string $usuario
 * @property string $contrasena
 * @property string $email
 * @property string $tipo
 *
 * @property Canciones[] $canciones
 */
class Usuarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['usuario', 'contrasena', 'email', 'tipo'], 'required'],
            [['tipo'], 'string'],
            [['usuario'], 'string', 'max' => 45],
            [['contrasena'], 'string', 'max' => 255],
            [['email'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idusuario' => 'Idusuario',
            'usuario' => 'Usuario',
            'contrasena' => 'Contrasena',
            'email' => 'Email',
            'tipo' => 'Tipo',
        ];
    }

    /**
     * Gets query for [[Canciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCanciones()
    {
        return $this->hasMany(Canciones::class, ['idusuario' => 'idusuario']);
    }
}

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Instrumentos $model */

$this->title = $model->idinstrumento;
$this->params['breadcrumbs'][] = ['label' => 'Instrumentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="instrumentos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idinstrumento' => $model->idinstrumento], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idinstrumento' => $model->idinstrumento], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idinstrumento',
            'tipo',
            'marca',
            'modelo',
            'tamaño',
            'material',
            'url:url',
        ],
    ]) ?>

</div>

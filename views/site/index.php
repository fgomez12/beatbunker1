<?php

/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>    

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">BeatBunker</h1>
    </div>

    <div class="body-content">        
        

        <div class="row">
            <a class="col-lg-4" href="index.php/usuarios/index">
                <div class="btn btn-outline-secondary"><h2>USUARIOS</h2></div>
            </a>
            <a class="col-lg-4" href="index.php/canciones/index">
                <div class="btn btn-outline-secondary"><h2>CANCIONES</h2></div>
            </a>
            <a class="col-lg-4" href="index.php/instrumentos/index">
                <div class="btn btn-outline-secondary"><h2>INSTRUMENTOS</h2></div>
            </a>
        </div>
        <div class="row">
            <a class="col-lg-4" href="index.php/generos/index">
                <div class="btn btn-outline-secondary"><h2>GÉNEROS</h2></div>
            </a>
            <a class="col-lg-4" href="index.php/suenan/index">
                <div class="btn btn-outline-secondary"><h2>SUENAN</h2></div>
            </a>
            <a class="col-lg-4" href="index.php/estudios/index">
                <div class="btn btn-outline-secondary"><h2>ESTUDIOS</h2></div>
            </a>
        </div>
        <div class="row">
            <a class="col-lg-4" href="index.php/utilizan/index">
                <div class="btn btn-outline-secondary"><h2>UTILIZAN</h2></div>
            </a>
            
            
        </div>
    </div>


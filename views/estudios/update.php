<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Estudios $model */

$this->title = 'Update Estudios: ' . $model->idestudio;
$this->params['breadcrumbs'][] = ['label' => 'Estudios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idestudio, 'url' => ['view', 'idestudio' => $model->idestudio]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="estudios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

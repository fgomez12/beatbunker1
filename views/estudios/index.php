<?php

use app\models\Estudios;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Estudios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estudios-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Estudios', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idestudio',
            'nombre',
            'web',
            'tecnico_sonido',
            'idcancion',
            //'idherramienta',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Estudios $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idestudio' => $model->idestudio]);
                 }
            ],
        ],
    ]); ?>


</div>

<?php

use app\models\Utilizan;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Utilizans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="utilizan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Utilizan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idutilizan',
            'idestudio',
            'idherramienta',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Utilizan $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idutilizan' => $model->idutilizan]);
                 }
            ],
        ],
    ]); ?>


</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Suenan $model */

$this->title = $model->idsuenan;
$this->params['breadcrumbs'][] = ['label' => 'Suenans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="suenan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idsuenan' => $model->idsuenan], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idsuenan' => $model->idsuenan], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idsuenan',
            'idinstrumento',
            'idcancion',
        ],
    ]) ?>

</div>

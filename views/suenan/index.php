<?php

use app\models\Suenan;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Suenans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="suenan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Suenan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idsuenan',
            'idinstrumento',
            'idcancion',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Suenan $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idsuenan' => $model->idsuenan]);
                 }
            ],
        ],
    ]); ?>


</div>

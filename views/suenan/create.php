<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Suenan $model */

$this->title = 'Create Suenan';
$this->params['breadcrumbs'][] = ['label' => 'Suenans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="suenan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

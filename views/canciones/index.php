<?php

use app\models\Canciones;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Canciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="canciones-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Canciones', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idcancion',
            'titulo',
            'album',
            'url_video:url',
            'interprete',
            //'idusuario',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Canciones $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idcancion' => $model->idcancion]);
                 }
            ],
        ],
    ]); ?>


</div>

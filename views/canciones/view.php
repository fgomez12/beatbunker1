<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Canciones $model */

$this->title = $model->idcancion;
$this->params['breadcrumbs'][] = ['label' => 'Canciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="canciones-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idcancion' => $model->idcancion], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idcancion' => $model->idcancion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idcancion',
            'titulo',
            'album',
            'url_video:url',
            'interprete',
            'idusuario',
        ],
    ]) ?>

</div>

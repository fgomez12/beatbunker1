﻿-- Base de datos: beatbunker

CREATE DATABASE IF NOT EXISTS beatbunker /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */;

USE beatbunker;

-- Tabla Usuarios: Almacena la información de los usuarios que tienen acceso al sistema.
CREATE TABLE Usuarios (
  idusuario INT NOT NULL AUTO_INCREMENT PRIMARY KEY, -- Identificador único para cada usuario
  usuario VARCHAR(45) NOT NULL, -- Nombre de usuario
  contrasena VARCHAR(255) NOT NULL, -- Contraseña (se recomienda almacenar de manera segura, por ejemplo, usando hash)
  email VARCHAR(100) NOT NULL, -- Correo electrónico del usuario
  tipo ENUM('admin','usuario') NOT NULL -- Tipo de usuario: admin (administrador) o usuario normal
 );

-- Tabla Canciones: Almacena la información de las canciones en la base de datos.
CREATE TABLE Canciones (
  idcancion INT NOT NULL AUTO_INCREMENT PRIMARY KEY, -- Identificador único para cada canción
  titulo VARCHAR(255) NOT NULL, -- Título de la canción
  album VARCHAR(255) NOT NULL, -- Nombre del álbum al que pertenece la canción
  url_video VARCHAR(255) NOT NULL, -- URL del video de la canción (si hay)
  interprete VARCHAR(45), -- Nombre del intérprete o artista
  idusuario INT NOT NULL, -- ID del usuario que agregó la canción
  FOREIGN KEY (idusuario) REFERENCES Usuarios(idusuario) -- Clave foránea que referencia al usuario que agregó la canción
);

-- Tabla Generos: Tabla multivaluada para asociar géneros musicales a canciones.
CREATE TABLE Generos (
  idgenero INT NOT NULL AUTO_INCREMENT PRIMARY KEY, -- Identificador único del género
  idcancion INT NOT NULL, -- ID de la canción asociada
  genero VARCHAR(50) NOT NULL, -- Género musical
  FOREIGN KEY (idcancion) REFERENCES Canciones(idcancion) -- Clave foránea que referencia a la canción
);

-- Tabla Instrumentos: Almacena información sobre los instrumentos utilizados en el proceso de grabación.
CREATE TABLE Instrumentos (
  idinstrumento INT NOT NULL AUTO_INCREMENT PRIMARY KEY, -- Identificador único para cada instrumento
  tipo VARCHAR(255) NOT NULL, -- Tipo de instrumento
  marca VARCHAR(45) DEFAULT NULL, -- Marca del instrumento
  modelo VARCHAR(45) DEFAULT NULL, -- Modelo del instrumento
  tamaño DOUBLE DEFAULT NULL, -- Tamaño del instrumento
  material VARCHAR(255) DEFAULT NULL, -- Material del que está hecho el instrumento
  url VARCHAR(255) -- URL del instrumento
);

-- Tabla Suenan: Asocia instrumentos a canciones para indicar qué instrumentos se utilizaron en cada canción.
CREATE TABLE Suenan (
  idsuenan INT NOT NULL AUTO_INCREMENT PRIMARY KEY, -- Identificador único para cada asociación instrumento-canción
  idinstrumento INT NOT NULL, -- ID del instrumento utilizado
  idcancion INT NOT NULL, -- ID de la canción en la que se utilizó el instrumento
  FOREIGN KEY (idinstrumento) REFERENCES Instrumentos(idinstrumento), -- Clave foránea que referencia a la tabla Instrumentos
  FOREIGN KEY (idcancion) REFERENCES Canciones(idcancion) -- Clave foránea que referencia a la tabla Canciones
);

-- Tabla Herramientas: Almacena información sobre herramientas utilizadas en el proceso de grabación.
CREATE TABLE Herramientas (
  idherramienta INT NOT NULL AUTO_INCREMENT PRIMARY KEY, -- Identificador único para cada herramienta
  nombre VARCHAR(100) NOT NULL, -- Nombre de la herramienta
  tipo ENUM('hardware','software') NOT NULL, -- Tipo de herramienta: hardware o software
  descripcion TEXT, -- Descripción de la herramienta
  url VARCHAR(255) -- URL de la herramienta (si está disponible en línea)
);

-- Tabla Estudios: Almacena información sobre estudios de grabación donde se realizan las grabaciones.
CREATE TABLE Estudios (
  idestudio INT NOT NULL AUTO_INCREMENT PRIMARY KEY, -- Identificador único para cada estudio
  nombre VARCHAR(255) NOT NULL, -- Nombre del estudio
  web VARCHAR(255), -- Sitio web del estudio (si está disponible)
  tecnico_sonido VARCHAR(70), -- Nombre del técnico de sonido del estudio
  idcancion INT NOT NULL, -- ID de la canción grabada en el estudio
  FOREIGN KEY (idcancion) REFERENCES Canciones(idcancion) -- Clave foránea que referencia a la tabla Canciones
);

-- Tabla Utilizan: Tabla intermedia para la relación N:N entre Estudios y Herramientas.
CREATE TABLE Utilizan (
  idutilizan INT NOT NULL AUTO_INCREMENT PRIMARY KEY, -- Identificador único para cada asociación estudio-herramienta
  idestudio INT NOT NULL, -- ID del estudio
  idherramienta INT NOT NULL, -- ID de la herramienta
  FOREIGN KEY (idestudio) REFERENCES Estudios(idestudio), -- Clave foránea que referencia a la tabla Estudios
  FOREIGN KEY (idherramienta) REFERENCES Herramientas(idherramienta) -- Clave foránea que referencia a la tabla Herramientas
);

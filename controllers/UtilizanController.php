<?php

namespace app\controllers;

use app\models\Utilizan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UtilizanController implements the CRUD actions for Utilizan model.
 */
class UtilizanController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Utilizan models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Utilizan::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'idutilizan' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Utilizan model.
     * @param int $idutilizan Idutilizan
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idutilizan)
    {
        return $this->render('view', [
            'model' => $this->findModel($idutilizan),
        ]);
    }

    /**
     * Creates a new Utilizan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Utilizan();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'idutilizan' => $model->idutilizan]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Utilizan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idutilizan Idutilizan
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idutilizan)
    {
        $model = $this->findModel($idutilizan);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idutilizan' => $model->idutilizan]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Utilizan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idutilizan Idutilizan
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idutilizan)
    {
        $this->findModel($idutilizan)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Utilizan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idutilizan Idutilizan
     * @return Utilizan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idutilizan)
    {
        if (($model = Utilizan::findOne(['idutilizan' => $idutilizan])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

<?php

namespace app\controllers;

use app\models\Instrumentos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * InstrumentosController implements the CRUD actions for Instrumentos model.
 */
class InstrumentosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Instrumentos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Instrumentos::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'idinstrumento' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Instrumentos model.
     * @param int $idinstrumento Idinstrumento
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idinstrumento)
    {
        return $this->render('view', [
            'model' => $this->findModel($idinstrumento),
        ]);
    }

    /**
     * Creates a new Instrumentos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Instrumentos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'idinstrumento' => $model->idinstrumento]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Instrumentos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idinstrumento Idinstrumento
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idinstrumento)
    {
        $model = $this->findModel($idinstrumento);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idinstrumento' => $model->idinstrumento]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Instrumentos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idinstrumento Idinstrumento
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idinstrumento)
    {
        $this->findModel($idinstrumento)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Instrumentos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idinstrumento Idinstrumento
     * @return Instrumentos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idinstrumento)
    {
        if (($model = Instrumentos::findOne(['idinstrumento' => $idinstrumento])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

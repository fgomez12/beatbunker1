<?php

namespace app\controllers;

use app\models\Suenan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SuenanController implements the CRUD actions for Suenan model.
 */
class SuenanController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Suenan models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Suenan::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'idsuenan' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Suenan model.
     * @param int $idsuenan Idsuenan
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idsuenan)
    {
        return $this->render('view', [
            'model' => $this->findModel($idsuenan),
        ]);
    }

    /**
     * Creates a new Suenan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Suenan();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'idsuenan' => $model->idsuenan]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Suenan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idsuenan Idsuenan
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idsuenan)
    {
        $model = $this->findModel($idsuenan);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idsuenan' => $model->idsuenan]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Suenan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idsuenan Idsuenan
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idsuenan)
    {
        $this->findModel($idsuenan)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Suenan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idsuenan Idsuenan
     * @return Suenan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idsuenan)
    {
        if (($model = Suenan::findOne(['idsuenan' => $idsuenan])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
